package com.bancarelvalentin.glrcbot.common

enum class IDENTITY(val isTheSuperiorOne: Boolean) {
    TRUFFE(true),
    NALA(false),
    AVAST(false);
}
