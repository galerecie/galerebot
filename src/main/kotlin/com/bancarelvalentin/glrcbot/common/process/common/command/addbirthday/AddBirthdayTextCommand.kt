package com.bancarelvalentin.glrcbot.common.process.common.command.addbirthday

import com.bancarelvalentin.ezbot.config.ConfigHandler
import com.bancarelvalentin.ezbot.default.features.liveconfig.LiveConfigHandlerBot
import com.bancarelvalentin.ezbot.process.command.Command
import com.bancarelvalentin.ezbot.process.command.param.CommandParam
import com.bancarelvalentin.ezbot.process.command.request.CommandRequest
import com.bancarelvalentin.ezbot.process.command.response.CommandResponse
import com.bancarelvalentin.glrcbot.common.CommonUtils
import com.bancarelvalentin.glrcbot.common.GlrcLiveConfig
import com.bancarelvalentin.glrcbot.common.GlrcSimpleLocalizeEnum
import com.bancarelvalentin.glrcbot.exception.WrongBirthdayDateFormatException
import java.util.Calendar
import java.util.function.BiConsumer

class AddBirthdayTextCommand : Command() {
    
    override val rawName = CommonUtils.localize(GlrcSimpleLocalizeEnum.COMMAND_DOC_CMD__ADD_BIRTHDAY__NAME)
    override val rawDesc = CommonUtils.localize(GlrcSimpleLocalizeEnum.COMMAND_DOC_CMD__ADD_BIRTHDAY__DESC)
    override val sample = CommonUtils.localize(GlrcSimpleLocalizeEnum.COMMAND_DOC_CMD__ADD_BIRTHDAY__SAMPLE)
    
    override val patterns = arrayOf("add_birhday", "ab")
    
    override val paramsClasses: Array<Class<out CommandParam<out Any?>>> =
        arrayOf(UserParam::class.java, NameParam::class.java, DateParam::class.java)
    
    override val logic = BiConsumer { request: CommandRequest, _: CommandResponse ->
        val user = request.getParamUser(0)!!
        val name = request.getParamString(1)!!
        val dateStr = request.getParamString(2)!!
        
        val date = try {
            val arr = dateStr.split("-")
            val instance = Calendar.getInstance()
            instance.set(Calendar.YEAR, arr[0].toInt())
            instance.set(Calendar.MONTH, arr[1].toInt() - 1)
            instance.set(Calendar.DAY_OF_MONTH, arr[2].toInt())
            instance.set(Calendar.HOUR, 0)
            instance.set(Calendar.MINUTE, 0)
            instance.set(Calendar.SECOND, 0)
            instance.set(Calendar.MILLISECOND, 0)
            instance.time
        } catch (e: java.lang.Exception) {
            throw WrongBirthdayDateFormatException(dateStr)
        }
        
        val liveConfig = ConfigHandler.liveConfig as GlrcLiveConfig
        liveConfig.birthdays = listOf(*liveConfig.birthdays.toTypedArray(), GlrcLiveConfig.Birthday(user.idLong, name, date))
        LiveConfigHandlerBot.saveInChat()
    }
}