package com.bancarelvalentin.glrcbot.common.process.common.background.onwelcomebot

import com.bancarelvalentin.ezbot.config.ConfigHandler
import com.bancarelvalentin.ezbot.process.EventListener
import com.bancarelvalentin.ezbot.process.Process
import com.bancarelvalentin.glrcbot.common.CommonConfig
import com.bancarelvalentin.glrcbot.common.CommonUtils
import com.bancarelvalentin.glrcbot.common.GlrcSpecificSimpleLocalizeEnum
import com.bancarelvalentin.glrcbot.common.HardCodedValues
import com.bancarelvalentin.glrcbot.common.IDENTITY
import net.dv8tion.jda.api.events.guild.member.GuildMemberJoinEvent


class OnWelcomeBotOnJoin(process: Process) : EventListener(process) {
    
    override fun onGuildMemberJoin(event: GuildMemberJoinEvent) {
        val guild = event.guild
        if ((ConfigHandler.config as CommonConfig).identity == IDENTITY.TRUFFE) Thread.sleep(1000)
        guild.getTextChannelById(HardCodedValues.CHANNEL__WELCOME)
            ?.sendMessage(CommonUtils.localize(GlrcSpecificSimpleLocalizeEnum.PROCESS__ON_WELCOME__RESPONSE, event.user, guild.getRoleById(HardCodedValues.ROLE__CO_CAPITAINE) ?: "", guild.getRoleById(HardCodedValues.ROLE__MAITRE_EQUIPAGE) ?: ""))
            ?.queue()
    }
}

