package com.bancarelvalentin.glrcbot.common.process.common.command.addbirthday

import com.bancarelvalentin.ezbot.process.command.param.StringCommandParam
import com.bancarelvalentin.glrcbot.common.CommonUtils
import com.bancarelvalentin.glrcbot.common.GlrcSimpleLocalizeEnum

class DateParam : StringCommandParam() {
    override val rawName = CommonUtils.localize(GlrcSimpleLocalizeEnum.COMMAND_DOC_CMD_PARAM__ADD_BIRTHDAY__DATE__NAME)
    override val rawDesc = CommonUtils.localize(GlrcSimpleLocalizeEnum.COMMAND_DOC_CMD_PARAM__ADD_BIRTHDAY__DATE__DESC)
}
