package com.bancarelvalentin.glrcbot.common.process.unique.background.genericinvitebot

import com.bancarelvalentin.ezbot.Orchestrator
import com.bancarelvalentin.ezbot.i18n.Localizator
import com.bancarelvalentin.ezbot.process.EventListener
import com.bancarelvalentin.ezbot.process.Process
import com.bancarelvalentin.ezbot.utils.ChannelUtils
import com.bancarelvalentin.ezbot.utils.EmojiEnum
import com.bancarelvalentin.glrcbot.common.GlrcSimpleLocalizeEnum
import com.bancarelvalentin.glrcbot.common.HardCodedValues
import com.bancarelvalentin.glrcbot.common.process.common.command.gameinvite.GameInviteTextCommand
import com.bancarelvalentin.glrcbot.common.process.unique.background.gamegrantsbot.GameGrantsHelper
import net.dv8tion.jda.api.EmbedBuilder
import net.dv8tion.jda.api.events.guild.GuildReadyEvent

class GenericInviteBotOnGuildReady(process: Process) : EventListener(process) {
    
    
    override fun onGuildReady(event: GuildReadyEvent) {
        if (event.guild.idLong == HardCodedValues.GUILD__GLRC) {
            val inviteChannel = gateway.getTextChannelById(HardCodedValues.CHANNEL__INVITE)!!
            ChannelUtils.emptyChannel(inviteChannel)
            
            inviteChannel.sendMessage(Localizator.get(GlrcSimpleLocalizeEnum.GENERIC_INVITE_INTRO_MESSAGE, EmojiEnum.RAISED_HAND.tounicode(), EmojiEnum.MICROPHONE.tounicode())).queue()
            
            GameGrantsHelper.init(gateway)
            GameGrantsHelper.selfAssignaleRoles
                .sortedBy { it.name }
                .forEach {
                    val embed = EmbedBuilder()
                        .setDescription(it.asMention)
                        .build()
                    inviteChannel.sendMessageEmbeds(embed).queue { message ->
                        message.addReaction(EmojiEnum.RAISED_HAND.tounicode()).queue()
                        message.addReaction(EmojiEnum.MICROPHONE.tounicode()).queue()
                        
                        process.addReactionEvent(message, null) { user, _, event ->
                            when (event.reactionEmote.emoji) {
                                EmojiEnum.MICROPHONE.tounicode() -> Orchestrator.getCommand(GameInviteTextCommand::class.java).createTemporaryChannel(event.guild, it)
                                EmojiEnum.RAISED_HAND.tounicode() -> Orchestrator.getCommand(GameInviteTextCommand::class.java).trigger(event.guild, event.guild.retrieveMember(user).complete(), it, null)
                            }
                            event.reaction.removeReaction(user).queue()
                        }
                        
                    }
                }
        }
    }
}

