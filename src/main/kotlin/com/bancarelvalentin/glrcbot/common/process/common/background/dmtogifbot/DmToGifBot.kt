package com.bancarelvalentin.glrcbot.common.process.common.background.dmtogifbot


import com.bancarelvalentin.ezbot.process.EventListener
import com.bancarelvalentin.ezbot.process.background.BackgroundProcess
import com.bancarelvalentin.glrcbot.common.CommonUtils
import com.bancarelvalentin.glrcbot.common.GlrcSimpleLocalizeEnum


class DmToGifBot : BackgroundProcess() {
    
    override val rawName = CommonUtils.localize(GlrcSimpleLocalizeEnum.PROCESS_DOC__DM_TO_GIF__NAME)
    override val rawDesc = CommonUtils.localize(GlrcSimpleLocalizeEnum.PROCESS_DOC__DM_TO_GIF__DESC)
    
    override fun getListeners(): Array<EventListener> {
        return arrayOf(DmToGifBotOnMessage(this))
    }
}