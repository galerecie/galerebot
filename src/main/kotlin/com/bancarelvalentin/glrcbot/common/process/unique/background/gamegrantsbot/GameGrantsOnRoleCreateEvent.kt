package com.bancarelvalentin.glrcbot.common.process.unique.background.gamegrantsbot

import com.bancarelvalentin.ezbot.process.EventListener
import com.bancarelvalentin.ezbot.process.Process
import net.dv8tion.jda.api.events.role.RoleCreateEvent


class GameGrantsOnRoleCreateEvent(process: Process) : EventListener(process) {
    override fun onRoleCreate(event: RoleCreateEvent) {
        GameGrantsHelper.roleTrigger(event.role)
    }
}
