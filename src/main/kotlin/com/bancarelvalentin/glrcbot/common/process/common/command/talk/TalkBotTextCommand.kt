package com.bancarelvalentin.glrcbot.common.process.common.command.talk

import com.bancarelvalentin.ezbot.process.command.Command
import com.bancarelvalentin.ezbot.process.command.param.CommandParam
import com.bancarelvalentin.ezbot.process.command.request.CommandRequest
import com.bancarelvalentin.ezbot.process.command.response.CommandResponse
import com.bancarelvalentin.glrcbot.common.CommonUtils
import com.bancarelvalentin.glrcbot.common.GlrcSimpleLocalizeEnum
import com.bancarelvalentin.glrcbot.common.HardCodedValues
import java.util.function.BiConsumer

class TalkBotTextCommand : Command() {
    
    override val rawName = CommonUtils.localize(GlrcSimpleLocalizeEnum.COMMAND_DOC_CMD__TALK__NAME)
    override val rawDesc = CommonUtils.localize(GlrcSimpleLocalizeEnum.COMMAND_DOC_CMD__TALK__DESC)
    override val sample = CommonUtils.localize(GlrcSimpleLocalizeEnum.COMMAND_DOC_CMD__TALK__SAMPLE)
    
    override val whitelistChannelIds = arrayOf(HardCodedValues.CHANNEL__BOT_COMMANDS_ADMIN)
    override val whitelistRoleIds = arrayOf(HardCodedValues.ROLE__MAITRE_EQUIPAGE)
    override val patterns = arrayOf("talk")
    override val paramsClasses: Array<Class<out CommandParam<out Any?>>> =
        arrayOf(ChannelParam::class.java, MessageParam::class.java)
    
    override val logic = BiConsumer { request: CommandRequest, _: CommandResponse ->
        val channel = request.getParamChannel(0)!!
        val msg = request.joinParams(1)!!
        channel.sendMessage(msg).queue()
    }
}
