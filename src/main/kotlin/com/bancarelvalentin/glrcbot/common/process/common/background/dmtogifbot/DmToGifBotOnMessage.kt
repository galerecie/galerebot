package com.bancarelvalentin.glrcbot.common.process.common.background.dmtogifbot

import at.mukprojects.giphy4j.Giphy
import at.mukprojects.giphy4j.exception.GiphyException
import com.bancarelvalentin.ezbot.config.ConfigHandler
import com.bancarelvalentin.ezbot.process.EventListener
import com.bancarelvalentin.ezbot.process.Process
import com.bancarelvalentin.glrcbot.common.CommonConfig
import com.bancarelvalentin.glrcbot.common.CommonUtils
import com.bancarelvalentin.glrcbot.common.GlrcSimpleLocalizeEnum
import net.dv8tion.jda.api.entities.ChannelType
import net.dv8tion.jda.api.events.message.MessageReceivedEvent


class DmToGifBotOnMessage(process: Process) : EventListener(process) {
    
    @Suppress("ObjectPropertyName", "PrivatePropertyName")
    private val RESULT_COUNT = 10
    
    override fun onMessageReceived(event: MessageReceivedEvent) {
        super.onMessageReceived(event)
        if (!event.message.isFromType(ChannelType.PRIVATE)) return
        if (event.author.isBot) return
        val answer = try {
            val results = Giphy((ConfigHandler.config as CommonConfig).giphyApiKey)
                .search(event.message.contentDisplay, RESULT_COUNT, 0)
                .dataList
            
            if (results.size > 0) {
                val randomResultIndex = (0 until results.size).random()
                results[randomResultIndex].images.original.url
            } else {
                CommonUtils.localize(GlrcSimpleLocalizeEnum.PROCESS_RETURN__DM_TO_GIF__NOTHING_FOUND)
            }
        } catch (e: GiphyException) {
            CommonUtils.localize(GlrcSimpleLocalizeEnum.PROCESS_RETURN__DM_TO_GIF__GIPHY_ERROR)
        }
        event.message.channel.sendMessage(answer).queue()
    }
}


