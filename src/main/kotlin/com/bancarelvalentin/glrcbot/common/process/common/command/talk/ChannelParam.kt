package com.bancarelvalentin.glrcbot.common.process.common.command.talk

import com.bancarelvalentin.ezbot.process.command.param.ChannelCommandParam
import com.bancarelvalentin.glrcbot.common.CommonUtils
import com.bancarelvalentin.glrcbot.common.GlrcSimpleLocalizeEnum

class ChannelParam : ChannelCommandParam() {
    override val rawName = CommonUtils.localize(GlrcSimpleLocalizeEnum.COMMAND_DOC_CMD_PARAM__TALK__CHANNEL__NAME)
    override val rawDesc = CommonUtils.localize(GlrcSimpleLocalizeEnum.COMMAND_DOC_CMD_PARAM__TALK__CHANNEL__DESC)
}