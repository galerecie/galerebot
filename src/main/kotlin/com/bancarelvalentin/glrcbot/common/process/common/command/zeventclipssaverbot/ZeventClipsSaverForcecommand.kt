package com.bancarelvalentin.glrcbot.common.process.common.command.zeventclipssaverbot

import com.bancarelvalentin.ezbot.process.command.Command
import com.bancarelvalentin.ezbot.process.command.request.CommandRequest
import com.bancarelvalentin.ezbot.process.command.response.CommandResponse
import com.bancarelvalentin.glrcbot.common.CommonUtils
import com.bancarelvalentin.glrcbot.common.GlrcSimpleLocalizeEnum
import com.bancarelvalentin.glrcbot.common.HardCodedValues
import com.bancarelvalentin.glrcbot.common.process.common.background.zeventclipssaverbot.ZeventClipsSaverHelper
import java.util.function.BiConsumer

class ZeventClipsSaverForcecommand : Command() {
    
    override val rawName = CommonUtils.localize(GlrcSimpleLocalizeEnum.COMMAND_DOC_CMD__FORCE_EVENT_CLIP_SAVE__NAME)
    override val rawDesc = CommonUtils.localize(GlrcSimpleLocalizeEnum.COMMAND_DOC_CMD__FORCE_EVENT_CLIP_SAVE__DESC)
    
    override val whitelistChannelIds = arrayOf(HardCodedValues.CHANNEL__BOT_COMMANDS_ADMIN)
    override val whitelistRoleIds = arrayOf(HardCodedValues.ROLE__MAITRE_EQUIPAGE)
    override val patterns = arrayOf("zeventclipsforce", "zcf")
    
    override val logic = BiConsumer { _: CommandRequest, _: CommandResponse ->
        ZeventClipsSaverHelper.tryAll(gateway)
    }
}

