package com.bancarelvalentin.glrcbot.common.process.unique.background.gamegrantsbot

import com.bancarelvalentin.ezbot.config.ConfigHandler
import com.bancarelvalentin.ezbot.process.EventListener
import com.bancarelvalentin.ezbot.process.Process
import com.bancarelvalentin.glrcbot.common.CommonConfig
import com.bancarelvalentin.glrcbot.common.HardCodedValues
import net.dv8tion.jda.api.entities.Category
import net.dv8tion.jda.api.entities.Role
import net.dv8tion.jda.api.entities.TextChannel
import net.dv8tion.jda.api.events.ReadyEvent


class GameGrantsOnReadyEvent(process: Process) : EventListener(process) {
    
    override fun onReady(event: ReadyEvent) {
        GameGrantsHelper.init(process.gateway)
        createBaseMessageIfNeeded()
        GameGrantsHelper.updateRoleList()
        checkChannelsPermissiosn()
        process.logger.info("Role assignment init over")
    }
    
    private fun createBaseMessageIfNeeded() {
        val existingMsgs = GameGrantsHelper.getAllMappedMessages()
        if (existingMsgs.size == 0) {
            GameGrantsHelper.assignationChannel.sendMessage(GameGrantsHelper.INTRODUCTION).queue()
        }
    }
    
    private fun checkChannelsPermissiosn() {
        val txtCat = process.gateway.getCategoryById(HardCodedValues.CATEGORY_GAMES_TEXT_CHATS)!!
        val patchCat = process.gateway.getCategoryById(HardCodedValues.CATEGORY_GAMES_PATCH_CHATS)!!
        
        GameGrantsHelper.selfAssignaleRoles.forEach { role ->
            var safeRoleName = role.name.replace(Regex("\\W"), "")
            safeRoleName = safeRoleName.substring(0, safeRoleName.length - HardCodedValues.playerRoleSuffix.length)
            setCategorysTextChannelsPermissions(txtCat, patchCat, safeRoleName, role)
        }
    }
    
    private fun setCategorysTextChannelsPermissions(txtCat: Category, patchCat: Category, safeRoleName: String, role: Role) {
        arrayOf(txtCat, patchCat).forEach { category ->
            category.channels.filterIsInstance<TextChannel>()
                .forEach { channel ->
                    val safeChannelName = channel.name.replace(Regex("\\W"), "")
                    val isGameChat = safeChannelName.contains(safeRoleName, ignoreCase = true)
                    if (isGameChat) {
                        process.logger.info("Reset @${role.name} permission on ${channel.javaClass.simpleName} ${role.name}")
                        channel.manager.sync().queue()
                        var override = channel.permissionContainer.rolePermissionOverrides.filter { permissionOverride -> permissionOverride.role!!.id == role.id }.getOrNull(0)
                        if (override == null) {
                            override = channel.permissionContainer.createPermissionOverride(role).complete()!!
                        }
                        override.manager.setAllow((ConfigHandler.config as CommonConfig).viewPermissionBit).queue()
                    }
                }
        }
    }
}
