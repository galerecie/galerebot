package com.bancarelvalentin.glrcbot.common.process.unique.background.gamegrantsbot

import com.bancarelvalentin.ezbot.config.ConfigHandler
import com.bancarelvalentin.ezbot.utils.Constants
import com.bancarelvalentin.glrcbot.common.CommonConfig
import com.bancarelvalentin.glrcbot.common.CommonUtils
import com.bancarelvalentin.glrcbot.common.GlrcSimpleLocalizeEnum
import com.bancarelvalentin.glrcbot.common.HardCodedValues
import net.dv8tion.jda.api.JDA
import net.dv8tion.jda.api.entities.Message
import net.dv8tion.jda.api.entities.Role
import net.dv8tion.jda.api.entities.TextChannel
import java.util.stream.Collectors

class GameGrantsHelper {
    companion object {
        private var gateway: JDA? = null
        val INTRODUCTION = CommonUtils.localize(GlrcSimpleLocalizeEnum.PROCESS__GAME_GRANTS__CHANNEL_INTRO, Constants.EMOTE_SUCCESS, Constants.EMOTE_ERROR)
        lateinit var assignationChannel: TextChannel
        public lateinit var selfAssignaleRoles: List<Role>
            private set
        
        private val extraRoleIds = mapOf(
            Pair("898721186249314354", CommonUtils.localize(GlrcSimpleLocalizeEnum.PROCESS__GAME_GRANTS__ROLE_DESC_18)), // 18+
            Pair("898721190321995836", CommonUtils.localize(GlrcSimpleLocalizeEnum.PROCESS__GAME_GRANTS__ROLE_DESC_DEV, Constants.EMOTE_SUCCESS, Constants.EMOTE_ERROR)),// dev
            Pair("930598353237082142", CommonUtils.localize(GlrcSimpleLocalizeEnum.PROCESS__GAME_GRANTS__ROLE_DESC_BOARD_GAME, Constants.EMOTE_SUCCESS, Constants.EMOTE_ERROR)),// Board game player
            Pair("930598357003546634", CommonUtils.localize(GlrcSimpleLocalizeEnum.PROCESS__GAME_GRANTS__ROLE_DESC_PYJAMA_PARTY, Constants.EMOTE_SUCCESS, Constants.EMOTE_ERROR))// Pyjama party player
        )
        
        
        private fun init() {
            init(gateway!!)
        }
        
        fun init(gateway: JDA) {
            Companion.gateway = gateway
            assignationChannel = gateway.getTextChannelById(HardCodedValues.CHANNEL__ROLE_ASSIGNEMENT)!!
            selfAssignaleRoles = gateway.getGuildById(HardCodedValues.GUILD__GLRC)!!.roles
                .stream()
                .filter { role -> isPlayerRole(role) }
                .collect(Collectors.toList())
        }
        
        private fun isPlayerRole(role: Role): Boolean {
            return (role.color == (ConfigHandler.config as CommonConfig).playerRoleColor && role.name.endsWith(HardCodedValues.playerRoleSuffix))
                    || extraRoleIds.containsKey(role.id)
        }
        
        fun getAllMappedMessages(): MutableList<Message> {
            return assignationChannel.history.retrievePast(100).complete().toMutableList()
        }
        
        private fun mapNewMessage(role: Role) {
            val msg = "${role.asMention} ${if (extraRoleIds.containsKey(role.id)) " : ${extraRoleIds[role.id]}" else ""}"
            assignationChannel.sendMessage(msg).queue {
                it.addReaction(Constants.EMOTE_SUCCESS.tounicode()).queue()
                it.addReaction(Constants.EMOTE_ERROR.tounicode()).queue()
            }
        }
        
        fun roleTrigger(role: Role) {
            if (isPlayerRole(role) && !isRoleMapped(role)) {
                mapNewMessage(role)
            }
        }
        
        private fun isRoleMapped(role: Role): Boolean {
            for (msg in getAllMappedMessages()) {
                val msgRolesIds = msg.mentionedRoles.map { r -> r.id }
                if (msgRolesIds.contains(role.id)) {
                    return true
                }
            }
            return false
        }
        
        fun getOneRole(nameContains: String): Role? {
            return selfAssignaleRoles
                .filter { it.name.contains(nameContains, ignoreCase = true) }
                .getOrNull(0)
        }
        
        fun updateRoleList() {
            init()
            val mappedMessages = getAllMappedMessages()
            
            val existingroles = selfAssignaleRoles.toMutableList()
            val itMsgs = mappedMessages.listIterator()
            
            while (itMsgs.hasNext()) {
                val msg = itMsgs.next()
                val itRoles = existingroles.iterator()
                val msgRolesIds = msg.mentionedRoles.map { role -> role.id }
                
                while (itRoles.hasNext()) {
                    val role = itRoles.next()
                    
                    if (msgRolesIds.contains(role.id)) {
                        itMsgs.remove()
                        itRoles.remove()
                        continue
                    }
                }
            }
            
            for (role in existingroles) {
                mapNewMessage(role)
            }
            for (msg in mappedMessages) {
                if (msg.contentRaw != INTRODUCTION) {
                    msg.delete().queue()
                }
            }
        }
    }
}