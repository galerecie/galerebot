package com.bancarelvalentin.glrcbot.common

import com.bancarelvalentin.ezbot.i18n.I18nDictionary

enum class GlrcSimpleLocalizeEnum : I18nDictionary {
    PROCESS_DOC__DM_TO_GIF__NAME,
    PROCESS_DOC__DM_TO_GIF__DESC,
    PROCESS_RETURN__DM_TO_GIF__NOTHING_FOUND,
    PROCESS_RETURN__DM_TO_GIF__GIPHY_ERROR,
    
    PROCESS_DOC__LOG_DMS__NAME,
    PROCESS_DOC__LOG_DMS__DESC,
    
    PROCESS_DOC__DELETE_TEMP_CHANNEL__NAME,
    PROCESS_DOC__DELETE_TEMP_CHANNEL__DESC,
    
    PROCESS_DOC__MENTION_REPLY__NAME,
    PROCESS_DOC__MENTION_REPLY__DESC,
    
    PROCESS_DOC__ON_BASTARD_MSG__NAME,
    PROCESS_DOC__ON_BASTARD_MSG__DESC,
    
    PROCESS_DOC__ON_WELCOME__NAME,
    PROCESS_DOC__ON_WELCOME__DESC,
    
    PROCESS_DOC__EVENT_CLIP_SAVER__NAME,
    PROCESS_DOC__EVENT_CLIP_SAVER__DESC,
    
    PROCESS_DOC__GENERIC_INVITE__NAME,
    PROCESS_DOC__GENERIC_INVITE__DESC,
    GENERIC_INVITE_INTRO_MESSAGE,
    
    PROCESS_DOC__BIRTHDAY__NAME,
    PROCESS_DOC__BIRTHDAY__DESC,
    
    PROCESS_DOC__GAME_GRANTS__NAME,
    PROCESS_DOC__GAME_GRANTS__DESC,
    PROCESS__GAME_GRANTS__CHANNEL_INTRO,
    PROCESS__GAME_GRANTS__ROLE_DESC_18,
    PROCESS__GAME_GRANTS__ROLE_DESC_DEV,
    PROCESS__GAME_GRANTS__ROLE_DESC_BOARD_GAME,
    PROCESS__GAME_GRANTS__ROLE_DESC_PYJAMA_PARTY,
    
    COMMAND_DOC_CMD__GAME_INVITE__NAME,
    COMMAND_DOC_CMD__GAME_INVITE__DESC,
    COMMAND_DOC_CMD__GAME_INVITE__SAMPLE,
    COMMAND_DOC_CMD_PARAM__GAME_INVITE__ROLE__NAME,
    COMMAND_DOC_CMD_PARAM__GAME_INVITE__ROLE__DESC,
    COMMAND_DOC_CMD_PARAM__GAME_INVITE__MESSAGE__NAME,
    COMMAND_DOC_CMD_PARAM__GAME_INVITE__MESSAGE__DESC,
    COMMAND_RETURN__GAME_INVITE__SUCCESS,
    COMMAND_RETURN__GAME_INVITE__EMBED_DESC,
    COMMAND_RETURN__GAME_INVITE__EMBED_FOOTER,
    COMMAND_RETURN__GAME_INVITE__EMBED_JOIN_VOCAL,
    COMMAND_RETURN__GAME_INVITE__ON_JOIN_MSG,
    COMMAND_RETURN__GAME_INVITE__ON_JOIN_TROLL_MSG,
    
    COMMAND_DOC_CMD__ADD_BIRTHDAY__NAME,
    COMMAND_DOC_CMD__ADD_BIRTHDAY__DESC,
    COMMAND_DOC_CMD__ADD_BIRTHDAY__SAMPLE,
    COMMAND_DOC_CMD_PARAM__ADD_BIRTHDAY__USER__NAME,
    COMMAND_DOC_CMD_PARAM__ADD_BIRTHDAY__USER__DESC,
    COMMAND_DOC_CMD_PARAM__ADD_BIRTHDAY__NAME__NAME,
    COMMAND_DOC_CMD_PARAM__ADD_BIRTHDAY__NAME__DESC,
    COMMAND_DOC_CMD_PARAM__ADD_BIRTHDAY__DATE__NAME,
    COMMAND_DOC_CMD_PARAM__ADD_BIRTHDAY__DATE__DESC,
    COMMAND_DOC_ERROR__ADD_BIRTHDAY__WRONG_DATE_FORMAT,
    
    COMMAND_DOC_CMD__GAME_SUPPORT_ADD__NAME,
    COMMAND_DOC_CMD__GAME_SUPPORT_ADD__DESC,
    COMMAND_DOC_CMD__GAME_SUPPORT_ADD__SAMPLE,
    COMMAND_DOC_CMD__GAME_SUPPORT_REMOVE__NAME,
    COMMAND_DOC_CMD__GAME_SUPPORT_REMOVE__DESC,
    COMMAND_DOC_CMD__GAME_SUPPORT_REMOVE__SAMPLE,
    COMMAND_DOC_CMD_PARAM__GAME_SUPPORT_ADD__ROLE_NAME,
    COMMAND_DOC_CMD_PARAM__GAME_SUPPORT_ADD__ROLE_DESC,
    COMMAND_DOC_CMD_PARAM__GAME_SUPPORT_ADD__CHANNEL__NAME,
    COMMAND_DOC_CMD_PARAM__GAME_SUPPORT_ADD__CHANNEL__DESC,
    COMMAND_DOC_CMD_PARAM__GAME_SUPPORT_DEL__ROLE_NAME,
    COMMAND_DOC_CMD_PARAM__GAME_SUPPORT_DEL__ROLE_DESC,
    COMMAND_RETURN__GAME_SUPPORT_ADD__ANOUNCEMENT,
    COMMAND_RETURN__GAME_SUPPORT_ADD__PATCHBOT_REMINDER,
    
    COMMAND_DOC_CMD__TALK__NAME,
    COMMAND_DOC_CMD__TALK__DESC,
    COMMAND_DOC_CMD__TALK__SAMPLE,
    COMMAND_DOC_CMD_PARAM__TALK__MESSAGE__NAME,
    COMMAND_DOC_CMD_PARAM__TALK__MESSAGE__DESC,
    COMMAND_DOC_CMD_PARAM__TALK__CHANNEL__NAME,
    COMMAND_DOC_CMD_PARAM__TALK__CHANNEL__DESC,
    
    COMMAND_DOC_CMD__REPLY__NAME,
    COMMAND_DOC_CMD__REPLY__DESC,
    COMMAND_DOC_CMD__REPLY__SAMPLE,
    COMMAND_DOC_CMD_PARAM__REPLY__CHANNEL__NAME,
    COMMAND_DOC_CMD_PARAM__REPLY__CHANNEL__DESC,
    COMMAND_DOC_CMD_PARAM__REPLY__MESSAGE_ID__NAME,
    COMMAND_DOC_CMD_PARAM__REPLY__MESSAGE_ID__DESC,
    COMMAND_DOC_CMD_PARAM__REPLY__MESSAGE__NAME,
    COMMAND_DOC_CMD_PARAM__REPLY__MESSAGE__DESC,
    
    COMMAND_DOC_CMD__REACT__NAME,
    COMMAND_DOC_CMD__REACT__DESC,
    COMMAND_DOC_CMD__REACT__SAMPLE,
    COMMAND_DOC_CMD_PARAM__REACT__CHANNEL__NAME,
    COMMAND_DOC_CMD_PARAM__REACT__CHANNEL__DESC,
    COMMAND_DOC_CMD_PARAM__REACT__MESSAGE_ID__NAME,
    COMMAND_DOC_CMD_PARAM__REACT__MESSAGE_ID__DESC,
    COMMAND_DOC_CMD_PARAM__REACT__EMOTE__NAME,
    COMMAND_DOC_CMD_PARAM__REACT__EMOTE__DESC,
    
    COMMAND_DOC_CMD__FORCE_EVENT_CLIP_SAVE__NAME,
    COMMAND_DOC_CMD__FORCE_EVENT_CLIP_SAVE__DESC,
    ;
    
    override val key: String = "common.${I18nDictionary.enumToKey(this)}"
}
